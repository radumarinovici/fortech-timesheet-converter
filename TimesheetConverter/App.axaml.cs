using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Converter;
using Converter.Models;
using Splat;
using TimesheetConverter.ViewModels;
using TimesheetConverter.Views;

namespace TimesheetConverter
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow();

                var spreadsheetConverterService = Locator.Current.GetRequiredService<ISpreadsheetConverterService>();
                var mySettingsConfig = Locator.Current.GetRequiredService<IMySettingsConfig>();

                desktop.MainWindow.DataContext = new MainWindowViewModel(desktop.MainWindow, spreadsheetConverterService, mySettingsConfig);
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}
