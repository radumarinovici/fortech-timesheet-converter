﻿using Avalonia.Controls.ApplicationLifetimes;
using Converter;
using Splat;
using System;
using TimesheetConverter.ViewModels;

namespace TimesheetConverter
{
    public static class Bootstrapper
    {
        public static void Register(IMutableDependencyResolver services, IReadonlyDependencyResolver resolver)
        {
            services.Register<ISpreadsheetConverterService>(() => new SpreadsheetConverterService());
        }

        public static void RegisterConstant<T>(T value)
        {
            Locator.CurrentMutable.RegisterConstant(value);
        }

        public static TService GetRequiredService<TService>(this IReadonlyDependencyResolver resolver)
        {
            var service = resolver.GetService<TService>();
            if (service is null) // Splat is not able to resolve type for us
            {
                throw new InvalidOperationException($"Failed to resolve object of type {typeof(TService)}"); // throw error with detailed description
            }

            return service; // return instance if not null
        }
    }
}
