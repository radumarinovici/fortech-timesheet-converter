using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Converter;
using Converter.Models;
using Splat;
using TimesheetConverter.ViewModels;

namespace TimesheetConverter.Views
{
    public partial class MainWindow : Window
    {
        private readonly MainWindowViewModel vm;
        public MainWindow()
        {
            InitializeComponent();

#if DEBUG
            this.AttachDevTools();
#endif
            var spreadsheetConverterService = Locator.Current.GetRequiredService<ISpreadsheetConverterService>();
            var mySettingsConfig = Locator.Current.GetRequiredService<IMySettingsConfig>();
            vm = new MainWindowViewModel(this, spreadsheetConverterService, mySettingsConfig);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
