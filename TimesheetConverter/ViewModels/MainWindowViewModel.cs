using Avalonia;
using Avalonia.Controls;
using Avalonia.Input.Platform;
using Converter;
using Converter.Models;
using OfficeOpenXml;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace TimesheetConverter.ViewModels
{
    public class MainWindowViewModel : ViewModelBase, IMainWindowViewModel
    {
        private readonly OpenFileDialog _openFileDialog;
        private readonly Window _window;
        private readonly ISpreadsheetConverterService _spreadsheetConverterService;
        private readonly IMySettingsConfig _mySettingsConfig;
        public IClipboard Clipboard { get; }
        public string SELECT_TIMESHEET_TEXT => "Copy timesheet path";
        private const string CONVERT_ENABLED_TEXT = "Open raw timesheet";
        private const string CONVERT_DISABLED_TEXT = "Timesheet converted";
        private bool _timesheetSelectorEnabled;
        private string _username;
        private bool _canConvertTimesheet;
        private string _conversionExceptionMessage;
        private bool _hasConversionException;
        public bool TimesheetSelectorEnabled
        {
            get => _timesheetSelectorEnabled;
            set => this.RaiseAndSetIfChanged(ref _timesheetSelectorEnabled, value);
        }
        private string _convertedTimesheetPath;
        public string ConvertedTimesheetPath
        {
            get => _convertedTimesheetPath;
            set => this.RaiseAndSetIfChanged(ref _convertedTimesheetPath, value);
        }
        public string Username
        {
            get => _username;
            set => this.RaiseAndSetIfChanged(ref _username, value);
        }
        public bool CanConvertTimesheet
        {
            get => _canConvertTimesheet;
            set
            {
                this.RaiseAndSetIfChanged(ref _canConvertTimesheet, value);
                this.RaisePropertyChanged(nameof(ConvertTimesheetText));
            }
        }
        public string ConversionExceptionMessage
        {
            get => _conversionExceptionMessage;
            set => this.RaiseAndSetIfChanged(ref _conversionExceptionMessage, value);
        }
        public bool HasConversionException
        {
            get => _hasConversionException;
            set
            {
                this.RaiseAndSetIfChanged(ref _hasConversionException, value);
                this.RaisePropertyChanged(nameof(ConvertTimesheetText));
            }
        }
        public string ConvertTimesheetText
        {
            get
            {
                if (HasConversionException)
                    return CONVERT_ENABLED_TEXT;

                return CanConvertTimesheet ? CONVERT_ENABLED_TEXT : CONVERT_DISABLED_TEXT;
            }
        }
        public MainWindowViewModel(Window window, ISpreadsheetConverterService spreadsheetConverterService, IMySettingsConfig mySettingsConfig)
        {
            _window = window;
            _spreadsheetConverterService = spreadsheetConverterService;
            _mySettingsConfig = mySettingsConfig;

            _openFileDialog = new OpenFileDialog
            {
                AllowMultiple = false,
                Filters = new List<FileDialogFilter>
                {
                    new FileDialogFilter
                    {
                        Name = "CSV files",
                        Extensions = new List<string> { "csv" }
                    }
                },
                Title = "Select your timesheet"
            };

            _username = _mySettingsConfig.FortechUsername;
            _canConvertTimesheet = true;
            Clipboard = ((IClipboard)AvaloniaLocator.Current.GetService(typeof(IClipboard)));
        }

        public async Task OnButtonClickCommand()
        {
            var selectedFilePath = (await _openFileDialog.ShowAsync(_window)).FirstOrDefault();
            if (selectedFilePath is null)
                return;
            ExcelPackage excelPackage = null;
            try
            {
                excelPackage = await _spreadsheetConverterService.Load(selectedFilePath);

                var provider = _mySettingsConfig.Providers.First();
                var charReplaceMap = provider.CharReplaceMap.ToDictionary(x => x.With, y => y.Replace.AsEnumerable());
                var valueFormatters = provider.ValueFormatters;

                var columnConverterOptions = new List<IColumnConverterOptions>
            {
                new ColumnConverterOptions
                {
                    ColumnNumber = 1,
                    ColumnName = "Item Id"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 2,
                    ColumnName = "Item Type"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 3,
                    ColumnName = "Item Name"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 4,
                    ColumnName = "User"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 5,
                    ColumnName = "Date"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 6,
                    ColumnName = "Worklog Type"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 7,
                    ColumnName = "Work Done"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 8,
                    ColumnName = "Project"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 9,
                    ColumnName = "Release"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 10,
                    ColumnName = "Description"
                },
                new ColumnConverterOptions
                {
                    ColumnNumber = 11,
                    ColumnName = "TaskType"
                }
            };

                var valueSanitizer = new ValueSanitizer(charReplaceMap);
                var valueProviders = new List<IValueProvider>
            {
                new CellValueProvider(excelPackage.Workbook.Worksheets[0], 1, 1, valueFormatters),
                new CellValueProvider(excelPackage.Workbook.Worksheets[0], 2, 18, valueFormatters),
                new SanitizedCellValueProvider(excelPackage.Workbook.Worksheets[0], 3, 2, valueSanitizer, valueFormatters),
                new StaticValueProvider(Username, 4),
                new CellValueProvider(excelPackage.Workbook.Worksheets[0], 5, 4, valueFormatters),
                new StaticValueProvider("Dev Hours", 6),
                new CellValueProvider(excelPackage.Workbook.Worksheets[0], 7, 3, valueFormatters),
                new StaticValueProvider("2.5_SECURRENCY_All", 8),
                new StaticValueProvider("Development Development", 9),
                new CellValueProvider(excelPackage.Workbook.Worksheets[0], 10, 24, valueFormatters),
                new CellValueProvider(excelPackage.Workbook.Worksheets[0], 11, 18, valueFormatters)
            };

                var convertedTimesheet = await _spreadsheetConverterService.Convert(excelPackage, new ConverterOptions(excelPackage.Workbook.Worksheets[0], provider.Name, charReplaceMap, columnConverterOptions, valueProviders));
                excelPackage.Dispose();
                var fileName = Path.GetFileNameWithoutExtension(selectedFilePath);
                var fileDirectory = Path.GetDirectoryName(selectedFilePath);
                var convertedFileName = $"{fileName}_converted_{DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss")}.csv";
                var convertedFilePath = Path.Combine(fileDirectory, convertedFileName);
                convertedTimesheet.SaveAsCsv(convertedFilePath);
                convertedTimesheet.Dispose();
                ConvertedTimesheetPath = convertedFilePath;
                TimesheetSelectorEnabled = true;
                await ShowConfirmation(2000);
            }
            catch (Exception ex)
            {
                await ShowErrorMessage(4000, ex.Message);
                return;
            }
        }

        public async Task OnCopyPath(string path)
        {
            await Clipboard.SetTextAsync(path);
        }

        public async Task ShowConfirmation(int delay)
        {
            CanConvertTimesheet = false;
            await Task.Delay(delay);
            CanConvertTimesheet = true;
        }
        public async Task ShowErrorMessage(int delay, string message)
        {
            CanConvertTimesheet = false;
            HasConversionException = true;
            ConversionExceptionMessage = message;
            await Task.Delay(delay);
            HasConversionException = false;
            ConversionExceptionMessage = "";
            CanConvertTimesheet = true;
        }
    }
}
