using Avalonia;
using Avalonia.ReactiveUI;
using Converter.Models;
using Microsoft.Extensions.Configuration;
using Splat;
using System;
using System.Diagnostics;
using System.IO;

namespace TimesheetConverter
{
    class Program
    {
        // Initialization code. Don't use any Avalonia, third-party APIs or any
        // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
        // yet and stuff might break.
        public static void Main(string[] args) => BuildAvaloniaApp()
            .StartWithClassicDesktopLifetime(args);

        // Avalonia configuration, don't remove; also used by visual designer.
        public static AppBuilder BuildAvaloniaApp()
        {
            RegisterDependencies();
            var here = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            if (string.IsNullOrEmpty(here)) throw new ApplicationException(
                "Could not retrieve the location of the running application");

            var configuration = new ConfigurationBuilder()
                .SetBasePath(here)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var mySettingsConfig = new MySettingsConfig();
            var mySettingsSection = configuration.GetSection("MySettings");
            mySettingsSection.Bind(mySettingsConfig);

            Bootstrapper.RegisterConstant<IMySettingsConfig>(mySettingsConfig);
            
            return AppBuilder.Configure<App>()
                .UsePlatformDetect()
                .LogToTrace()
                .UseReactiveUI();
        }

        private static void RegisterDependencies()
        {
            Bootstrapper.Register(Locator.CurrentMutable, Locator.Current);
        }
    }
}
