﻿using Converter.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Converter
{
    public class CellValueProvider : ICellValueProvider, IValueProvider
    {
        protected readonly ExcelWorksheet Worksheet;
        private readonly int _targetColumn;
        private readonly Dictionary<Type, string> _valueFormatters;
        public int SourceColumn { get; }
        public CellValueProvider(ExcelWorksheet worksheet, int targetColumn, int sourceColumn, IReadOnlyList<ValueFormatter> valueFormatters)
        {
            Worksheet = worksheet;
            _targetColumn = targetColumn;
            SourceColumn = sourceColumn;
            _valueFormatters = valueFormatters.ToDictionary(x => x.GetContainingType(), y => y.Format);
        }

        public int TargetColumnNumber() => _targetColumn;
        public virtual string GetValue(int row)
        {
            object value = Worksheet.Cells[row, SourceColumn].Value;

            if (value == null)
                return "";

            if (_valueFormatters.ContainsKey(value.GetType()))
            {
                var formatter = _valueFormatters.First(x => x.Key == value.GetType());
                
                if (value is DateTime dateTime)
                {
                    return EscapeQuotes((dateTime).ToString(formatter.Value));
                }
            }

            return EscapeQuotes(value.ToString());
        }

        private string EscapeQuotes(string input)
        {
            var result = input;
            foreach (var @char in new string[] { "\"", "'" })
            {
                result = result.Replace(@char, $"");
            }

            return result;
        }
    }
}
