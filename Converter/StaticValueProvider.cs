﻿namespace Converter
{
    public class StaticValueProvider : IStaticValueProvider, IValueProvider
    {
        private readonly string _value;
        private readonly int _column;
        public StaticValueProvider(string value, int column)
        {
            _value = value;
            _column = column;
        }

        public int TargetColumnNumber() => _column;

        public string GetValue() => _value;
    }
}
