﻿using System.Collections.Generic;

namespace Converter.Models
{
    public interface IMySettingsConfig
    {
        public string FortechUsername { get; }
        public IReadOnlyList<Provider> Providers { get; }
    }
}
