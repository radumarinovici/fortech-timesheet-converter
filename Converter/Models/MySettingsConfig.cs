﻿using System;
using System.Collections.Generic;

namespace Converter.Models
{
    public class MySettingsConfig : IMySettingsConfig
    {
        public string FortechUsername { get; set; }
        public IReadOnlyList<Provider> Providers { get; set; }
    }
    public class Provider
    {
        public string Name { get; set; }
        public IReadOnlyList<CharReplaceOption> CharReplaceMap { get; set; }
        public IReadOnlyList<ValueFormatter> ValueFormatters { get; set; }

    }
    public class ValueFormatter
    {
        public string Type { get; set; }
        public string Format { get; set; }

        private static IReadOnlyDictionary<string, Type> _formatters => new Dictionary<string, Type>
            {
                { nameof(DateTime), typeof(DateTime) }
            };
        public Type? GetContainingType()
        {
            return _formatters.ContainsKey(Type) ? _formatters[Type] : null;
        }
    }
    public class CharReplaceOption
    {
        public IReadOnlyList<string> Replace { get; set; }
        public string With { get; set; }
    }
}
