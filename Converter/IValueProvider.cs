﻿namespace Converter
{
    public interface IValueProvider
    {
        int TargetColumnNumber();
    }
}
