﻿namespace Converter
{
    public interface ICellValueProvider
    {
        string GetValue(int row);
    }
}
