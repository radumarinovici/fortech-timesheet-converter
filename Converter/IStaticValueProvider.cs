﻿namespace Converter
{
    public interface IStaticValueProvider
    {
        string GetValue();
    }
}
