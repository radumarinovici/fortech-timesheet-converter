﻿namespace Converter
{
    public class ColumnConverterOptions : IColumnConverterOptions
    {
        public int ColumnNumber { get; set; }

        public string ColumnName { get; set; }
    }
}
