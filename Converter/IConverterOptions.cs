﻿using OfficeOpenXml;
using System.Collections.Generic;

namespace Converter
{
    public interface IConverterOptions
    {
        ExcelWorksheet Source { get; }
        string ProviderName { get; }
        IDictionary<string, IEnumerable<string>> CharReplaceMap { get; }
        IReadOnlyCollection<IColumnConverterOptions> ColumnConverterOptions { get; }
        IReadOnlyCollection<IValueProvider> ValueProviders { get; }
    }
}
