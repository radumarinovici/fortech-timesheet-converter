﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Text;

namespace Converter
{
    public class ConverterOptions : IConverterOptions
    {

        public ConverterOptions(ExcelWorksheet worksheet, string providerName, IDictionary<string, IEnumerable<string>> charReplaceMap, IReadOnlyCollection<IColumnConverterOptions> columnConverterOptions, IReadOnlyCollection<IValueProvider> valueProviders)
        {
            Source = worksheet;
            ProviderName = providerName;
            CharReplaceMap = charReplaceMap;
            ColumnConverterOptions = columnConverterOptions;
            ValueProviders = valueProviders;
        }

        public ExcelWorksheet Source { get; }

        public string ProviderName { get; }

        public IDictionary<string, IEnumerable<string>> CharReplaceMap { get; }

        public IReadOnlyCollection<IColumnConverterOptions> ColumnConverterOptions { get; }

        public IReadOnlyCollection<IValueProvider> ValueProviders { get; }
    }
}
