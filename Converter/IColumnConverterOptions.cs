﻿namespace Converter
{
    public interface IColumnConverterOptions
    {
        int ColumnNumber { get; }
        string ColumnName { get; }
    }
}
