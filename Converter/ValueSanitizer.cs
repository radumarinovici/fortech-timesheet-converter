﻿using System.Collections.Generic;

namespace Converter
{
    public class ValueSanitizer : IValueSanitizer
    {
        private IDictionary<string, IEnumerable<string>> _charReplaceMap;
        public ValueSanitizer(IDictionary<string, IEnumerable<string>> charReplaceMap)
        {
            _charReplaceMap = charReplaceMap;
        }
        public string Sanitize(string input)
        {
            foreach (var replaceWith in _charReplaceMap.Keys)
            {
                foreach (var valueToReplace in _charReplaceMap[replaceWith])
                {
                    input = input.Replace(valueToReplace, replaceWith);
                }
            }

            return input;
        }
    }
}
