﻿using Converter.Models;
using OfficeOpenXml;
using System.Collections.Generic;

namespace Converter
{
    public class SanitizedCellValueProvider : CellValueProvider
    {
        private IValueSanitizer _valueSanitizer;
        public SanitizedCellValueProvider(ExcelWorksheet worksheet, int targetColumn, int sourceColumn, IValueSanitizer valueSanitizer, IReadOnlyList<ValueFormatter> valueFormatters) : base(worksheet, targetColumn, sourceColumn, valueFormatters)
        {
            _valueSanitizer = valueSanitizer;
        }

        public override string GetValue(int row)
        {
            var value = base.GetValue(row);
            if (value == "")
                return value;

            return _valueSanitizer.Sanitize(value);
        }
    }
}
