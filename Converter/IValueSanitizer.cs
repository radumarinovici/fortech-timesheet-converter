﻿using System.Collections.Generic;

namespace Converter
{
    public interface IValueSanitizer
    {
        string Sanitize(string input);
    }
}
