﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Converter
{
    public class SpreadsheetConverterService : ISpreadsheetConverterService
    {
        public async Task<ExcelPackage> Convert(ExcelPackage excelPackage, IConverterOptions options)
        {
            var excel = new ExcelPackage();
            excel.Workbook.Worksheets.Add("Timesheet");
            AddColumnDefinition(excel.Workbook.Worksheets[0], options.ColumnConverterOptions);
            AddData(excelPackage.Workbook.Worksheets[0], excel.Workbook.Worksheets[0], options);

            return excel;


            void AddColumnDefinition(ExcelWorksheet worksheet, IReadOnlyCollection<IColumnConverterOptions> columnConverterOptions)
            {
                foreach (var columnOption in columnConverterOptions)
                {
                    worksheet.Cells[1, columnOption.ColumnNumber].Value = columnOption.ColumnName;
                }
            }

            void AddData(ExcelWorksheet sourceWorksheet, ExcelWorksheet targetWorksheet, IConverterOptions options)
            {
                int sourceRows = sourceWorksheet.Dimension.Rows;
                foreach (var row in Enumerable.Range(2, sourceRows - 2))
                {
                    foreach (var valueProvider in options.ValueProviders)
                    {
                        if (valueProvider is ICellValueProvider)
                        {
                            targetWorksheet.Cells[row, valueProvider.TargetColumnNumber()].Value = ((ICellValueProvider)valueProvider).GetValue(row);
                        }
                        else if (valueProvider is IStaticValueProvider)
                        {
                            targetWorksheet.Cells[row, valueProvider.TargetColumnNumber()].Value = ((IStaticValueProvider)valueProvider).GetValue();
                        }
                    }
                }

            }
        }

        public async Task<ExcelPackage> Load(string path)
        {
            var pck = new ExcelPackage();
            var sheet = pck.Workbook.Worksheets.Add("Timesheet");
            var file = new FileInfo(path);
            var format = new OfficeOpenXml.ExcelTextFormat
            {
                Delimiter = ',',
                TextQualifier = '"'
            };
            await sheet.Cells["A1"].LoadFromTextAsync(file, format);
            return pck;
        }
    }
}