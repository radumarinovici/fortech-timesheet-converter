﻿using OfficeOpenXml;
using System.Threading.Tasks;

namespace Converter
{
    public interface ISpreadsheetConverterService
    {
        Task<ExcelPackage> Convert(ExcelPackage excelPackage, IConverterOptions options);
        Task<ExcelPackage> Load(string path);
    }
}
