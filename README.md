# README #

### How to use ###
* 1) Download Securrency timesheet as raw data->csv
* 2) (Temporary step) Open a new excel document and import data from step 1 (Data->From Text/CSV) and save the document as CSV.
* 3) Adjust appsettings.json from TimesheetConverter to use your Fortech username. Alternatively, run the executable and manually update it in the username field.
* 4) Select the timesheet from step 2. This will trigger the timesheet conversion, the result will be saved in the same folder and the name will be \*timesheetName\*\_converted\_\*currentDateTime\*.
* 5) Optionally you can copy to clipboard the path of the resulting timesheet.
* 6) Import the result in Fortech import tool